// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "GreenpOfferwall",
  products: [
    .library(
      name: "GreenpOfferwall",
      targets: [
        "GreenpOfferwall",
        "Flutter",
      ]
    ),
  ],
  targets: [
    .binaryTarget(
      name: "GreenpOfferwall",
      url: "https://gitlab.com/-/project/59497577/uploads/72a235c4ae56f24cd7f7946045722c7a/GreenpOfferwall.xcframework.zip",
      checksum: "85a07f58690d434d8e8b8761859ff77f9f7f90c40531f3a24601536081db8d81"
    ),
    .binaryTarget(
      name: "Flutter",
      url: "https://gitlab.com/-/project/59497577/uploads/6d13475d26913b98524e006901216adb/Flutter.xcframework.zip",
      checksum: "0ecb9bd47c16bf370f5f2137775628aeb429a62e8ac87970d9a5175fc1db3d56"
    ),
  ]
)
